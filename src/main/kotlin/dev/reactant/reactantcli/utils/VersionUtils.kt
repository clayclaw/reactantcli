package dev.reactant.reactantcli.utils

import org.json.JSONObject
import java.io.File


object VersionUtils {
    const val UNKNOWN = "Unknown"
    fun getCLIVersion(): String {
        return VersionUtils::class.java.`package`.implementationVersion
    }

    fun getTemplateVersion(templateInfo: File): String {
        if (!templateInfo.exists()) return "Unknown";
        val templateMinCLIVersion = JSONObject(templateInfo.readText()).getString("minCLIVersion");
        return templateMinCLIVersion
    }

    fun validateVersion(templateInfo: File): Boolean {
        val currentCLI = getCLIVersion().split(".").map { it.toInt() }
        val requiredCLIStr = getTemplateVersion(templateInfo);
        if (requiredCLIStr == UNKNOWN) return false;
        val requiredCLI = requiredCLIStr.split(".").map { it.toInt() }

        currentCLI.forEachIndexed { i, v ->
            if (requiredCLI.size <= i) return true
            if (requiredCLI[i] > v) return false;
            if (requiredCLI[i] < v) return true;
        }
        return true
    }
}
