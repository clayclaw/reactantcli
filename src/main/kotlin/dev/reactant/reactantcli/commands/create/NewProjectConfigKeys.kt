package dev.reactant.reactantcli.commands.create

enum class NewProjectConfigKeys(val key: String) {

    PROJECT_NAME("projectName"),
    MAIN_CLASS("mainClass"),
    GROUP("group"),
    VERSION("version"),

    BINTRAY("bintray"),
    BINTRAY_REPO("bintrayRepo"),

    SHADOWJAR("shadowjar"),
    AUTODEPLOY("autodeploy"),

    VCS_URL("vcsUrl"),

    PACKAGE("package"),
    MAIN_CLASS_NAME("mainClassName");
}
