package dev.reactant.reactantcli

import dev.reactant.reactantcli.commands.ReactantCommand
import picocli.CommandLine

fun main(args: Array<String>) {
    CommandLine(ReactantCommand())
        .execute(*args)
}

