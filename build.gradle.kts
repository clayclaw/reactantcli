import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "dev.reactant"
version = "0.1.3"

buildscript {
    repositories {
        jcenter()
        mavenCentral()
    }
    dependencies {
        classpath("com.github.jengelman.gradle.plugins:shadow:4.0.3")
    }
}


plugins {
    kotlin("jvm") version "1.3.21"
}
apply(plugin = "com.github.johnrengelman.shadow")

repositories {
    mavenCentral()
}

val shadowJar: ShadowJar by tasks
shadowJar.apply {
    baseName = project.name
    relocate("org.bstats", "dev.reactant.reactant.core")
    archiveName = "reactant-cli.jar"
}
tasks.getByName("build").dependsOn(shadowJar)

val zip = tasks.register<Zip>("distZip") {
    archiveName = "${project.name}.zip"
    from(shadowJar)
    from(file("src/main/script"))
}
tasks.getByName("build").dependsOn(zip)


dependencies {
    compile(kotlin("stdlib-jdk8"))
    compile("info.picocli:picocli:4.0.0-alpha-3")
    compile("org.eclipse.jgit:org.eclipse.jgit:5.3.1.201904271842-r")
    compile("ch.qos.logback:logback-classic:1.2.3")
    compile("me.tongfei:progressbar:0.7.4")
    compile("org.json:json:20180813")
    compile("org.freemarker:freemarker:2.3.29")
    compile("commons-io:commons-io:2.6")
    compile("org.apache.commons:commons-lang3:3.0");
    compile("commons-codec:commons-codec:1.13");
}

tasks.withType<Jar> {
    manifest {
        attributes(
            mapOf(
                "Main-Class" to "dev.reactant.reactantcli.ReactantCLIKt",
                "Implementation-Version" to version
            )
        )
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
