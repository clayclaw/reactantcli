package dev.reactant.reactantcli.commands

import dev.reactant.reactantcli.ManifestVersionProvider
import dev.reactant.reactantcli.commands.create.NewCommand
import dev.reactant.reactantcli.commands.generate.GenerateCommand
import picocli.CommandLine
import picocli.CommandLine.Command
import picocli.CommandLine.Option

@Command(
    name = "reactant",
    subcommands = [NewCommand::class, GenerateCommand::class],
    versionProvider = ManifestVersionProvider::class
)
class ReactantCommand : Runnable {

    @Option(names = ["-v", "--version"], versionHelp = true, description = ["Show version of cli"])
    var versionRequested: Boolean = false

    override fun run() {
        if (!versionRequested)
            CommandLine(this)
                .usage(System.out);
    }
}
